# ZSH Custom

This repo holds zsh customizations that I like. 

## Useage

This should be used in conjunction with [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh).
Set `$ZSH_CUSTOM` to the location of this repository (in oh-my-zsh's `.zshrc`) and oh-my-zsh will automatically source all the .zsh files here.

